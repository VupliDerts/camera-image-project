# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

bl_info = {
    "name" : "Camera Image Project",
    "author" : "Nicholas Masterton", 
    "description" : "A tool for quickly projecting an image onto an object from a camera object.",
    "blender" : (3, 0, 0),
    "version" : (1, 0, 0),
    "location" : "",
    "waring" : "",
    "doc_url": "", 
    "tracker_url": "", 
    "category" : "3D View" 
}


import bpy
import bpy.utils.previews


addon_keymaps = {}
_icons = None


def property_exists(prop_path, glob, loc):
    try:
        eval(prop_path, glob, loc)
        return True
    except:
        return False


class SNA_PT_CAMERA_IMAGE_PROJECT_6D0CB(bpy.types.Panel):
    bl_label = 'Camera Image Project'
    bl_idname = 'SNA_PT_CAMERA_IMAGE_PROJECT_6D0CB'
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_context = ''
    bl_category = 'CIP'
    bl_order = 0
    bl_ui_units_x=0

    @classmethod
    def poll(cls, context):
        return not (False)

    def draw_header(self, context):
        layout = self.layout

    def draw(self, context):
        layout = self.layout
        op = layout.operator('sna.project_image_09eaf', text='Project Image', icon_value=0, emboss=True, depress=False)
        if property_exists("bpy.context.view_layer.objects.selected[0]", globals(), locals()):
            layout.label(text=str(bpy.context.view_layer.objects.selected.keys()), icon_value=256)


class SNA_OT_Project_Image_09Eaf(bpy.types.Operator):
    bl_idname = "sna.project_image_09eaf"
    bl_label = "project_image"
    bl_description = "Project an image from the selected camera onto the selected object"
    bl_options = {"REGISTER", "UNDO"}

    @classmethod
    def poll(cls, context):
        return not False

    def execute(self, context):
        selobjs = bpy.context.selected_objects
        subd = 2
        if not len(selobjs) == 2:
            raise TypeError('please select two objects')
        for ob in selobjs:
            if ob.type == 'MESH':
                object = ob
            elif ob.type == 'CAMERA':
                camera = ob
            else:
                raise TypeError('only camera and mesh objects allowed')
        if not camera.data.background_images:
            raise TypeError('camera must have a background image')
        image = camera.data.background_images[0].image
        res = [image.size[0],image.size[1]]
        if not object.data.uv_layers:
            raise TypeError('object must have a UV map')
        uv = object.data.uv_layers[0]
        # add subsurf modifier
        object.modifiers.new('Image Project SubSurf','SUBSURF')
        object.modifiers["Image Project SubSurf"].subdivision_type = 'SIMPLE'
        object.modifiers['Image Project SubSurf'].levels = subd
        # add uv project modifier
        object.modifiers.new('Image Project','UV_PROJECT')
        object.modifiers["Image Project"].uv_layer = uv.name
        object.modifiers['Image Project'].projectors[0].object = camera
        object.modifiers["Image Project"].aspect_x = res[0]
        object.modifiers["Image Project"].aspect_y = res[1]
        # make material and append
        material = bpy.data.materials.new('Projection')
        object.data.materials.append(material)
        material.use_nodes = True
        tex = material.node_tree.nodes.new('ShaderNodeTexImage')
        tex.image = image
        tex.extension = 'CLIP'
        tex.location = [-750,250]
        principled = material.node_tree.nodes['Principled BSDF']
        mix = material.node_tree.nodes.new('ShaderNodeMixRGB')
        mix.location = [-325,250]
        material.node_tree.links.new(tex.outputs[0],mix.inputs[2])
        material.node_tree.links.new(tex.outputs[1],mix.inputs[0])
        material.node_tree.links.new(mix.outputs[0],principled.inputs[0])
        return {"FINISHED"}

    def invoke(self, context, event):
        return self.execute(context)


def register():
    global _icons
    _icons = bpy.utils.previews.new()
    bpy.utils.register_class(SNA_PT_CAMERA_IMAGE_PROJECT_6D0CB)
    bpy.utils.register_class(SNA_OT_Project_Image_09Eaf)


def unregister():
    global _icons
    bpy.utils.previews.remove(_icons)
    wm = bpy.context.window_manager
    kc = wm.keyconfigs.addon
    for km, kmi in addon_keymaps.values():
        km.keymap_items.remove(kmi)
    addon_keymaps.clear()
    bpy.utils.unregister_class(SNA_PT_CAMERA_IMAGE_PROJECT_6D0CB)
    bpy.utils.unregister_class(SNA_OT_Project_Image_09Eaf)
